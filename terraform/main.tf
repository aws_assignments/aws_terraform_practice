# # provider "aws" {
# #     region = "us-east-1"
# #     access_key = "AKIAZL47BOTPY7GYMI4Y"
# #     secret_key = "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"
# # }
#

# #S3 bucket
resource "aws_s3_bucket" "assign_bucket" {
  bucket = "sf-assign1"
  #acl    = "private"  # Access Control List for the bucket (private, public-read, etc.)
}

resource "aws_s3_bucket_object" "csv_object" {
  bucket = "sf-assign"
  key    = "test.csv"  # Specify the key (filename) of the CSV file in the bucket
  source = "${path.module}/test.csv"  # Path to the local CSV file you want to upload
}

# #Dynamodb table
resource "aws_dynamodb_table" "assign_table" {
  name     = "assign-table"
  billing_mode = "PROVISIONED"  # Change to "PAY_PER_REQUEST" for on-demand billing
  read_capacity = 5  # Adjust as needed
  write_capacity = 5  # Adjust as needed
  hash_key = "id"  # Change to the desired hash key name
  attribute {
    name = "id"
    type = "S"  # String type
  }
}

data "archive_file" "practice_lambda_zip" {
  type = "zip"
  source_file = "${path.module}/lambda.py"
  output_path = "${path.module}/lambda.zip"
}

# Lambda Function
resource "aws_lambda_function" "assign_lambda" {
  function_name = "sf_sample_lambda_function"  # Replace with your desired function name
  role          = aws_iam_role.lambda_role.arn
  handler       = "lambda_function.lambda_handler"
  runtime       = "python3.9"  # Change the runtime as per your requirement
  filename      = data.archive_file.practice_lambda_zip.output_path  # Replace with the path to your Lambda deployment package
  environment {
    variables = {
      DYNAMODB_TABLE_NAME = aws_dynamodb_table.assign_table.name
      S3_BUCKET_NAME      = aws_s3_bucket.assign_bucket.bucket
    }
  }
}

# # Lambda Function to modify data in DynamoDB
resource "aws_lambda_function" "modify_lambda" {
  function_name = "modify_lambda"
  runtime       = "python3.9"
  handler       = "lambda_function.modify_handler"
  filename      = data.archive_file.practice_lambda_zip.output_path
  role          = aws_iam_role.lambda_role.arn

  environment {
    variables = {
      DYNAMODB_TABLE_NAME = aws_dynamodb_table.assign_table.name
    }
  }
}

# Lambda Function to remove data from DynamoDB
resource "aws_lambda_function" "remove_lambda" {
  function_name = "remove_lambda"
  runtime       = "python3.9"
  handler       = "lambda_function.remove_handler"
  filename      = data.archive_file.practice_lambda_zip.output_path
  role          = aws_iam_role.lambda_role.arn

  environment {
    variables = {
      DYNAMODB_TABLE_NAME = aws_dynamodb_table.assign_table.name
    }
  }
}

# IAM Role for Lambda Function
resource "aws_iam_role" "lambda_role" {
  name = "sf_assign_lambda_role"  # Replace with your desired role name

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
        Action    = "sts:AssumeRole"
      }
    ]
  })
}

 resource "aws_api_gateway_rest_api" "example_api" {
  name        = "example_api"
  description = "Example API"
}

# Define the resource (e.g., /example)
resource "aws_api_gateway_resource" "example_resource" {
  rest_api_id = aws_api_gateway_rest_api.example_api.id
  parent_id   = aws_api_gateway_rest_api.example_api.root_resource_id
  path_part   = "retrieve-and-save"
}

# Define the HTTP method (e.g., GET)
resource "aws_api_gateway_method" "example_method" {
  rest_api_id   = aws_api_gateway_rest_api.example_api.id
  resource_id   = aws_api_gateway_resource.example_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

# Define the integration for the HTTP method
resource "aws_api_gateway_integration" "example_integration" {
  depends_on              = [aws_api_gateway_method.example_method]
  rest_api_id             = aws_api_gateway_rest_api.example_api.id
  resource_id             = aws_api_gateway_resource.example_resource.id
  http_method             = aws_api_gateway_method.example_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.assign_lambda.invoke_arn
}

resource "aws_lambda_permission" "api_lambda_permission" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.assign_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_rest_api.example_api.execution_arn}/*/*"
}

# # Define the deployment
resource "aws_api_gateway_deployment" "example_deployment" {
  depends_on = [aws_api_gateway_integration.example_integration]
  rest_api_id = aws_api_gateway_rest_api.example_api.id
  stage_name = "prod"
}

output "api_endpoint_url" {
  value = aws_api_gateway_deployment.example_deployment.invoke_url
}


# # Configure Terraform backend to store state in S3 bucket
# terraform {
#   backend "s3" {
#     bucket         = "sf-assign"
#     key            = "terraform.tfstate"
#     region         = "us-east-1"  # Update with your desired region
#     encrypt        = true
#     #dynamodb_table = "terraform_locks"  # Optional: Use DynamoDB for state locking
#   }
# }