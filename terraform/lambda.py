import boto3
import csv
import json


def lambda_handler(event, context):
    # Get the S3 bucket and object key from the event
    s3_bucket = event['Records'][0]['s3']['bucket']['name']
    s3_key = event['Records'][0]['s3']['object']['key']

    # Initialize S3 and DynamoDB clients
    s3 = boto3.client('s3')
    dynamodb = boto3.resource('dynamodb')

    # Retrieve the CSV file from S3
    response = s3.get_object(Bucket=s3_bucket, Key=s3_key)
    csv_data = response['Body'].read().decode('utf-8').splitlines()

    # Parse CSV data and write to DynamoDB table
    table = dynamodb.Table('assign-table')
    for row in csv.reader(csv_data):
        table.put_item(Item={
            'id': row[0],  # Assuming the first column is the ID
            'name': row[1],  # Assuming the second column is the name
            # Add additional attributes as needed
        })

    return {
        'statusCode': 200,
        'body': json.dumps('Data from CSV file inserted into DynamoDB table')
    }