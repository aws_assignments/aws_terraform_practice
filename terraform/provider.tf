# Configure Terraform backend to store state in S3 bucket
terraform {
  backend "s3" {
    bucket         = "sf-assign"
    key            = "terraform.tfstate"
    region         = "us-east-1"  # Update with your desired region
    encrypt        = true
    #dynamodb_table = "assign-table"  # Optional: Use DynamoDB for state locking
  }
}
# terraform {
#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = "~> 4.18.0"
#     }
#   }
#   backend "" {
#     workspaces {
#       name = "assign_ws"
#     }
#   }
# }
